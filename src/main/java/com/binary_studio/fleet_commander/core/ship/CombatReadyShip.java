package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger maxShieldHP;

	private PositiveInteger shieldHP;

	private PositiveInteger maxHullHP;

	private PositiveInteger hullHP;

	private PositiveInteger maxCapacitor;

	private PositiveInteger capacity;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(DockedShip.CreateInDock canBeCreatedOnlyInDock, String name, PositiveInteger maxShieldHP,
			PositiveInteger maxHullHP, PositiveInteger powergridOutput, PositiveInteger capacitorAmount,
			PositiveInteger capacitorRechargeRate, PositiveInteger speed, PositiveInteger size,
			AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.maxShieldHP = maxShieldHP;
		this.shieldHP = maxShieldHP;
		this.maxHullHP = maxHullHP;
		this.hullHP = maxHullHP;
		this.maxCapacitor = capacitorAmount;
		this.capacity = this.maxCapacitor;
		this.capacitorRegeneration = capacitorRechargeRate;
		this.pg = powergridOutput;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	@Override
	public void endTurn() {
		this.capacity = (this.capacity.value() + this.capacitorRegeneration.value() > this.maxCapacitor.value())
				? this.maxCapacitor : PositiveInteger.of(this.capacity.value() + this.capacitorRegeneration.value());
	}

	@Override
	public void startTurn() {
		// regenerate();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.attackSubsystem.getCapacitorConsumption().value() > this.capacity.value()) {
			return Optional.empty();
		}
		this.capacity = PositiveInteger
				.of(this.maxCapacitor.value() - this.attackSubsystem.getCapacitorConsumption().value());
		PositiveInteger damage = this.attackSubsystem.attack(target);
		AttackAction attackAction = new AttackAction(damage, this, target, this.attackSubsystem);
		return Optional.of(attackAction);
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction reducedAttack = this.defenciveSubsystem.reduceDamage(attack);
		int hp = this.shieldHP.value() - reducedAttack.damage.value();
		if (hp >= 0) {
			this.shieldHP = PositiveInteger.of(hp);
		}
		else {
			this.shieldHP = PositiveInteger.of(0);
			int hull = this.hullHP.value() + hp; // hp is negative
			if (hull > 0) {
				this.hullHP = PositiveInteger.of(hull);
			}
			else {
				return new AttackResult.Destroyed();
			}
		}
		return new AttackResult.DamageRecived(attack.weapon, reducedAttack.damage, this);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.defenciveSubsystem.getCapacitorConsumption().value() > this.capacity.value()) {
			return Optional.empty();
		}
		this.capacity = PositiveInteger
				.of(this.capacity.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
		RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
		int diffShieldHP = this.shieldHP.value();
		int diffHullHP = this.hullHP.value();
		this.shieldHP = (regenerateAction.shieldHPRegenerated.value() + this.shieldHP.value() >= this.maxShieldHP
				.value()) ? this.maxShieldHP
						: PositiveInteger.of(regenerateAction.shieldHPRegenerated.value() + this.shieldHP.value());
		this.hullHP = (regenerateAction.hullHPRegenerated.value() + this.hullHP.value() >= this.maxHullHP.value())
				? this.maxHullHP : PositiveInteger.of(regenerateAction.hullHPRegenerated.value() + this.hullHP.value());
		diffShieldHP = this.shieldHP.value() - diffShieldHP;
		diffHullHP = this.hullHP.value() - diffHullHP;
		return Optional.of(new RegenerateAction(PositiveInteger.of(diffShieldHP), PositiveInteger.of(diffHullHP)));
	}

}
