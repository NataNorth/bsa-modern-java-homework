package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private static String name;

	private static PositiveInteger shieldHP;

	private static PositiveInteger hullHP;

	private static PositiveInteger capacitor;

	private static PositiveInteger capacitorRegeneration;

	private static PositiveInteger pg;

	private static PositiveInteger speed;

	private static PositiveInteger size;

	private static AttackSubsystem attackSubsystem;

	private static DefenciveSubsystem defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) throws IllegalArgumentException {
		if (name == null || name.trim().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		DockedShip.name = name;
		DockedShip.shieldHP = shieldHP;
		DockedShip.hullHP = hullHP;
		DockedShip.capacitor = capacitorAmount;
		DockedShip.capacitorRegeneration = capacitorRechargeRate;
		DockedShip.pg = powergridOutput;
		DockedShip.speed = speed;
		DockedShip.size = size;
		attackSubsystem = null;
		defenciveSubsystem = null;
		return new DockedShip();
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			attackSubsystem = null;
		}
		else {
			int pgInWork = defenciveSubsystem == null ? 0 : defenciveSubsystem.getPowerGridConsumption().value();
			int freePg = pg.value() - pgInWork - subsystem.getPowerGridConsumption().value();
			if (freePg < 0) {
				throw new InsufficientPowergridException(Math.abs(freePg));
			}
			else {
				attackSubsystem = subsystem;
			}
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			defenciveSubsystem = null;
		}
		else {
			int pgInWork = attackSubsystem == null ? 0 : attackSubsystem.getPowerGridConsumption().value();
			int freePg = pg.value() - pgInWork - subsystem.getPowerGridConsumption().value();
			if (freePg < 0) {
				throw new InsufficientPowergridException(Math.abs(freePg));
			}
			else {
				defenciveSubsystem = subsystem;
			}
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (attackSubsystem == null && defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		return new CombatReadyShip(createInDock, name, shieldHP, hullHP, pg, capacitor, capacitorRegeneration, speed,
				size, attackSubsystem, defenciveSubsystem);
	}

	private static final CreateInDock createInDock = new CreateInDock();

	public static final class CreateInDock {

		private CreateInDock() {
		}

	} // let the instance CombatReadyShip be created only from dock

}
