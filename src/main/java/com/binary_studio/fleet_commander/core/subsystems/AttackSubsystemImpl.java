package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private static String name;

	private static PositiveInteger baseDamage;

	private static PositiveInteger optimalSize;

	private static PositiveInteger optimalSpeed;

	private static PositiveInteger capacitorConsumption;

	private static PositiveInteger pgRequirement;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name == null || name.trim().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		AttackSubsystemImpl.name = name;
		AttackSubsystemImpl.baseDamage = baseDamage;
		AttackSubsystemImpl.optimalSize = optimalSize;
		AttackSubsystemImpl.optimalSpeed = optimalSpeed;
		AttackSubsystemImpl.capacitorConsumption = capacitorConsumption;
		AttackSubsystemImpl.pgRequirement = powergridRequirments;
		return new AttackSubsystemImpl();
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier;
		if (target.getSize().value() >= optimalSize.value()) {
			sizeReductionModifier = 1;
		}
		else {
			sizeReductionModifier = (double) target.getSize().value() / optimalSize.value();
		}
		double speedReductionModifier;
		if (target.getCurrentSpeed().value() <= optimalSpeed.value()) {
			speedReductionModifier = 1;
		}
		else {
			speedReductionModifier = (double) optimalSpeed.value() / (2 * target.getCurrentSpeed().value());
		}
		int damage = (int) Math.ceil(baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier));
		return PositiveInteger.of(damage);
	}

	@Override
	public String getName() {
		return name;
	}

}
