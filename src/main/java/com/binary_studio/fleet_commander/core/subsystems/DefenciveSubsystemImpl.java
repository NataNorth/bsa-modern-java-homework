package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private static String name;

	private static PositiveInteger impactReduction;

	private static PositiveInteger shieldRegen;

	private static PositiveInteger hullRegen;

	private static PositiveInteger capacitorUsage;

	private static PositiveInteger pgRequirement;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name == null || name.trim().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		DefenciveSubsystemImpl.name = name;
		DefenciveSubsystemImpl.impactReduction = impactReductionPercent;
		DefenciveSubsystemImpl.shieldRegen = shieldRegeneration;
		DefenciveSubsystemImpl.hullRegen = hullRegeneration;
		DefenciveSubsystemImpl.capacitorUsage = capacitorConsumption;
		DefenciveSubsystemImpl.pgRequirement = powergridConsumption;
		return new DefenciveSubsystemImpl();
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return capacitorUsage;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		double reductionPercent = impactReduction.value() > 95 ? 0.95 : impactReduction.value() / 100.;
		double reduceDamage = incomingDamage.damage.value() - incomingDamage.damage.value() * reductionPercent;
		return new AttackAction(PositiveInteger.of((int) Math.ceil(reduceDamage)), incomingDamage.attacker,
				incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(shieldRegen, hullRegen);
	}

}
