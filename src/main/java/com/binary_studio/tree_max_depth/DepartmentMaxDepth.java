package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.Queue;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		Queue<Department> q = new LinkedList();
		q.add(rootDepartment);
		int height = 0;
		while (true) {
			int nodeCount = q.size();
			if (nodeCount == 0)
				return height;
			height++;
			while (nodeCount > 0)
			{
				Department newDepartment = q.peek();
				q.remove();
				if (newDepartment.subDepartments != null) {
					for (var child : newDepartment.subDepartments) {
						if (child != null) {
							q.add(child);
						}
					}
				}
				nodeCount--;
			}
		}
	}

}
